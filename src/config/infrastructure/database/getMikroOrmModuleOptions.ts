import { Configuration } from '@mikro-orm/core/utils/Configuration';
import { MikroOrmModuleOptions } from '@mikro-orm/nestjs';

import { DatabaseConfig } from './DatabaseConfig';

export function getMikroOrmModuleOptions(
  databaseConfig: DatabaseConfig,
  allowGlobalContext: boolean = false,
  platformType: keyof typeof Configuration.PLATFORMS = 'postgresql',
): MikroOrmModuleOptions {
  const mikroOrmModuleOptions: MikroOrmModuleOptions = {
    allowGlobalContext,
    autoLoadEntities: true,
    dbName: databaseConfig.database,
    debug: true,
    forceUndefined: true,
    host: databaseConfig.host,
    migrations: {
      path: 'dist/common/infrastructure/mikroOrm/migrations',
      pathTs: 'src/common/infrastructure/mikroOrm/migrations',
    },
    password: databaseConfig.password,
    port: databaseConfig.port,
    type: platformType,
    user: databaseConfig.user,
  };

  return mikroOrmModuleOptions;
}
