import path from 'path';

import { Injectable } from '@nestjs/common';
import dotenv from 'dotenv';
import * as envalid from 'envalid';

import { LoadDataAdapter } from '../../../domain/adapter/LoadDataAdapter';
import { EnvToEnvValidatorEnvalidMap } from '../../envalid/model/EnvToEnvValidatorEnvalidMap';

@Injectable()
export class LoadDataDotenvAdapter<TData> implements LoadDataAdapter<TData> {
  private readonly ENV_VARIABLES_DOTENV_DEFAULT_FILE_NAME: string = '';

  public constructor(private readonly envToEnvValidatorEnvalidMap: EnvToEnvValidatorEnvalidMap<TData>) {}

  public loadData(): TData {
    this.populateProcessEnv();

    const cleanEnvProxy: TData = this.cleanEnv();
    const cleanEnv: TData = { ...cleanEnvProxy };

    return cleanEnv;
  }

  private populateProcessEnv(): void {
    const dotenvOptions: dotenv.DotenvConfigOptions = {
      path: this.getEnvFilepath(),
    };

    dotenv.config(dotenvOptions);
  }

  private getEnvFilepath(): string {
    const dotenvName: string = process.env.ENV ?? this.ENV_VARIABLES_DOTENV_DEFAULT_FILE_NAME;

    const envFilepath: string = path.join(process.cwd(), `${dotenvName}.env`);

    return envFilepath;
  }

  private cleanEnv(): TData {
    const envalidOptions: envalid.CleanOptions<TData> = {};

    const cleanEnv: TData = envalid.cleanEnv(process.env, this.envToEnvValidatorEnvalidMap, envalidOptions);

    return cleanEnv;
  }
}
