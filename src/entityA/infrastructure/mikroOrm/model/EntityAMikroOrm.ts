import { Entity, Property } from '@mikro-orm/core';
import { BaseEntityMikroOrm } from '../../../../common/infrastructure/mikroOrm/model/BaseEntityMikroOrm';

@Entity({ tableName: 'EntityAMikroOrm' })
export class EntityAMikroOrm extends BaseEntityMikroOrm {
  @Property({ length: 64, name: 'name', type: 'varchar' })
  name!: string;
}
