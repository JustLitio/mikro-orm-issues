import { BaseEntity } from '../../../common/domain/model/BaseEntity';

export interface EntityA extends BaseEntity {
  name: string;
}
