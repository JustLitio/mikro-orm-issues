import * as envalid from 'envalid';

import { AppEnvVariables } from '../../../domain/model/AppEnvVariables';

export const appEnvVariablesToEnvVariablesValidatorEnvalidMap: {
  [TKey in keyof AppEnvVariables]: envalid.ValidatorSpec<AppEnvVariables[TKey]>;
} = {
  NODE_PORT: envalid.port(),
};
