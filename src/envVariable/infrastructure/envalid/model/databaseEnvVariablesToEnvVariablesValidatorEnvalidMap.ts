import * as envalid from 'envalid';

import { DatabaseEnvVariables } from '../../../domain/model/DatabaseEnvVariables';

export const databaseEnvVariablesToEnvVariablesValidatorEnvalidMap: {
  [TKey in keyof DatabaseEnvVariables]: envalid.ValidatorSpec<DatabaseEnvVariables[TKey]>;
} = {
  DB_DATABASE: envalid.str(),
  DB_HOST: envalid.host(),
  DB_PASSWORD: envalid.str(),
  DB_PORT: envalid.port(),
  DB_USER: envalid.str(),
};
