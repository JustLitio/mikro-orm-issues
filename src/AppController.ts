import { EntityRepository, RequiredEntityData } from '@mikro-orm/core';
import { Controller, Get } from '@nestjs/common';
import { randomUUID } from 'crypto';

import { EntityAMikroOrm } from './entityA/infrastructure/mikroOrm/model/EntityAMikroOrm';
import { EntityBMikroOrm } from './entityB/infrastructure/mikroOrm/model/EntityBMikroOrm';
import { InjectRepository } from '@mikro-orm/nestjs';

@Controller()
export class AppController {
  public constructor(
    @InjectRepository(EntityAMikroOrm)
    private readonly entityARepository: EntityRepository<EntityAMikroOrm>,
    @InjectRepository(EntityBMikroOrm)
    private readonly entityBRepository: EntityRepository<EntityBMikroOrm>,
  ) {}

  @Get('/seedForParallelUpdate')
  public async seedForParallelUpdate(): Promise<void> {
    const createdById: string = randomUUID();

    const insertOneQueryMikroOrmEntityA_1: RequiredEntityData<EntityAMikroOrm> = {
      createdById,
      name: 'entityA_1_before_parallel_update',
    };

    const insertOneQueryMikroOrmEntityA_2: RequiredEntityData<EntityAMikroOrm> = {
      createdById,
      name: 'entityA_2_before_parallel_update',
    };

    const insertOneQueryMikroOrmEntityA_3: RequiredEntityData<EntityAMikroOrm> = {
      createdById,
      name: 'entityA_3_before_parallel_update',
    };

    const entityA_1MikroOrm: EntityAMikroOrm = this.entityARepository.create(insertOneQueryMikroOrmEntityA_1);
    const entityA_2MikroOrm: EntityAMikroOrm = this.entityARepository.create(insertOneQueryMikroOrmEntityA_2);
    const entityA_3MikroOrm: EntityAMikroOrm = this.entityARepository.create(insertOneQueryMikroOrmEntityA_3);

    await this.entityARepository.persistAndFlush([entityA_1MikroOrm, entityA_2MikroOrm, entityA_3MikroOrm]);
  }

  @Get('/parallelUpdate')
  public async parallelUpdate(): Promise<void> {
    const updatedById: string = randomUUID();
  }

  @Get('/updateTriggeredAfterInsert')
  public async updateTriggeredAfterInsert(): Promise<void> {
    const createdById: string = randomUUID();

    const insertOneQueryMikroOrmEntityA_1: RequiredEntityData<EntityAMikroOrm> = {
      createdById,
      name: 'entityA_1',
    };

    let entityA_1MikroOrm: EntityAMikroOrm | null = this.entityARepository.create(insertOneQueryMikroOrmEntityA_1);

    await this.entityARepository.persistAndFlush(entityA_1MikroOrm);

    entityA_1MikroOrm = await this.entityARepository.findOne(
      { createdById, name: 'entityA_1' },
      { disableIdentityMap: true },
    );

    if (entityA_1MikroOrm !== null) {
      const insertOneQueryMikroOrmEntityB_1: RequiredEntityData<EntityBMikroOrm> = {
        createdById,
        entityA: entityA_1MikroOrm.id,
        name: 'entityB_1',
      };

      const entityB_1MikroOrm: EntityBMikroOrm = this.entityBRepository.create(insertOneQueryMikroOrmEntityB_1);

      await this.entityBRepository.persistAndFlush(entityB_1MikroOrm);
    }
  }
}
