import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Module } from '@nestjs/common';

import { AppController } from './AppController';
import { DatabaseConfig } from './config/infrastructure/database/DatabaseConfig';
import { getMikroOrmModuleOptions } from './config/infrastructure/database/getMikroOrmModuleOptions';
import { AppConfigModule } from './config/infrastructure/injection/AppConfigModule';
import { DatabaseConfigModule } from './config/infrastructure/injection/DatabaseConfigModule';
import { EntityAMikroOrm } from './entityA/infrastructure/mikroOrm/model/EntityAMikroOrm';
import { EntityBMikroOrm } from './entityB/infrastructure/mikroOrm/model/EntityBMikroOrm';

@Module({
  controllers: [AppController],
  imports: [
    AppConfigModule,
    MikroOrmModule.forFeature([EntityAMikroOrm]),
    MikroOrmModule.forFeature([EntityBMikroOrm]),
    MikroOrmModule.forRootAsync({
      imports: [DatabaseConfigModule],
      inject: [DatabaseConfig],
      useFactory: getMikroOrmModuleOptions,
    }),
  ],
})
export class AppModule {}
