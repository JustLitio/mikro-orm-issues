import { Entity, IdentifiedReference, ManyToOne, Property } from '@mikro-orm/core';
import { EntityAMikroOrm } from '../../../../entityA/infrastructure/mikroOrm/model/EntityAMikroOrm';
import { BaseEntityMikroOrm } from '../../../../common/infrastructure/mikroOrm/model/BaseEntityMikroOrm';

@Entity({ tableName: 'EntityBMikroOrm' })
export class EntityBMikroOrm extends BaseEntityMikroOrm {
  @Property({ length: 64, name: 'name', type: 'varchar' })
  name!: string;

  @ManyToOne(() => EntityAMikroOrm, {
    name: 'entityA_id',
    wrappedReference: true,
  })
  entityA!: IdentifiedReference<EntityAMikroOrm>;
}
