import { BaseEntity } from '../../../common/domain/model/BaseEntity';

export interface EntityB extends BaseEntity {
  entityAId: string;
  name: string;
}
