import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';

import { AppModule } from './AppModule';
import { AppConfig } from './config/infrastructure/app/AppConfig';

async function bootstrap(): Promise<void> {
  const adapter: FastifyAdapter = new FastifyAdapter({ logger: true });

  const app: NestFastifyApplication =
    await NestFactory.create<NestFastifyApplication>(AppModule, adapter, {
      cors: { origin: '*' },
    });

  app.enableShutdownHooks();

  const appConfig: AppConfig = app.get(AppConfig);

  await app.listen(appConfig.port, '0.0.0.0');
}

void bootstrap();
