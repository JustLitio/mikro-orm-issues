import { Migration } from '@mikro-orm/migrations';

export class Migration20230421120604 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table "EntityAMikroOrm" ("id" uuid not null, "created_at" timestamptz(3) not null, "created_by_id" uuid not null, "updated_at" timestamptz(3) null, "updated_by_id" uuid null, "version" int not null default 1, "name" varchar(64) not null, constraint "EntityAMikroOrm_pkey" primary key ("id"));');

    this.addSql('create table "EntityBMikroOrm" ("id" uuid not null, "created_at" timestamptz(3) not null, "created_by_id" uuid not null, "updated_at" timestamptz(3) null, "updated_by_id" uuid null, "version" int not null default 1, "name" varchar(64) not null, "entityA_id" uuid not null, constraint "EntityBMikroOrm_pkey" primary key ("id"));');

    this.addSql('alter table "EntityBMikroOrm" add constraint "EntityBMikroOrm_entityA_id_foreign" foreign key ("entityA_id") references "EntityAMikroOrm" ("id") on update cascade;');
  }

  async down(): Promise<void> {
    this.addSql('alter table "EntityBMikroOrm" drop constraint "EntityBMikroOrm_entityA_id_foreign";');

    this.addSql('drop table if exists "EntityAMikroOrm" cascade;');

    this.addSql('drop table if exists "EntityBMikroOrm" cascade;');
  }

}
