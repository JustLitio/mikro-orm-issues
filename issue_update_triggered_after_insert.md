**Describe the bug**

In the context of a single request in a Nest.js application:

When inserting an Entity A that has a relation to an Entity B, if I retrieve Entity B before inserting Entity A, an update query on Entity B gets triggered.

Is that behaviour intended?

**To reproduce**

Here's a repo where the issue can be reproduced:

(link to repo)

```

**Expected behaviour**

The insert query on Entity A does not trigger and update query on Entity B, whether or not a select query has been made on Entity B before

**Versions**

Dependency | Version
-- | --
node | v18.16.0
typescript | 4.9.5
nest-js | 9.4.0
mikro-orm | 5.6.16
driver | postgresql


