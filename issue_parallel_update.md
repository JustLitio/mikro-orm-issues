**Describe the bug**

In the context of a single request in a Nest.js application:

When launching multiple update queries in parallel on entities of the same type, the resulting aggregated update query is missing some params, and that results in a kind of partial update.

**To reproduce**

Here's a repo where the issue can be reproduced:

(link to repo)

```

**Expected behaviour**

Every entity gets updated correctly

**Versions**

Dependency | Version
-- | --
node | v18.16.0
typescript | 4.9.5
nest-js | 9.4.0
mikro-orm | 5.6.16
driver | postgresql


